package com.example.rmanganye.app.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rmanganye.app.R;
import com.example.rmanganye.app.framework.ClubApplication;
import com.example.rmanganye.app.framework.model.JoinResponse;
import com.example.rmanganye.app.framework.services.JoinService;
import com.example.rmanganye.app.framework.model.JoinModel;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Join extends AppCompatActivity {

    private EditText _etUsername;
    private EditText _etCellNumber;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
         sharedPreferences = getSharedPreferences("confirmed",0);
        boolean confirmed = sharedPreferences.getBoolean("confirmed",false);
        if(confirmed)
            startActivity(new Intent(Join.this, Dashboard.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        _etCellNumber = (EditText) findViewById(R.id.etCellNumber);
        _etUsername = (EditText) findViewById(R.id.etUsername);
        Button btn_join = (Button) findViewById(R.id.btn_join);

        btn_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = _etUsername.getText().toString();
                String cell = _etCellNumber.getText().toString();

                if (username.isEmpty() || cell.isEmpty()) {
                    Toast.makeText(getBaseContext(), "Provide all values", Toast.LENGTH_LONG).show();
                } else {

                    JoinModel model = new JoinModel(cell, username);
                    JoinService service = ClubApplication.getRetrofit().create(JoinService.class);
                    Call<JoinResponse> join = service.join(model);
                    join.enqueue(new Callback<JoinResponse>() {
                        @Override
                        public void onResponse(Call<JoinResponse> call, Response<JoinResponse> response) {
                            if(response.isSuccessful()){
                                Intent intent = saveDataAndGetIntent(response);
                                startActivity(intent);
                            }else{
                                AlertDialog.Builder builder =
                                        new AlertDialog.Builder(Join.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                                builder.setTitle("Error");
                                builder.setMessage("Registration failed");
                                builder.setPositiveButton("OK", null);
                                builder.setNegativeButton("Cancel", null);
                                builder.show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JoinResponse> call, Throwable t) {
                            Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });


                }
            }
        });
    }

    private Intent saveDataAndGetIntent(Response<JoinResponse> response) {
        Intent intent = new Intent(Join.this,ConfirmNumber.class);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("USER_ID",response.body().getId());
        editor.putString("USER_CELL",response.body().getCellNumber());
        editor.putString("USER_NAME",response.body().getUsername());
        editor.apply();
        intent.putExtra(ConfirmNumber.USER_ID,response.body().getId());
        intent.putExtra(ConfirmNumber.CONFIRM_CODE,response.body().getCode());
        return intent;
    }

    public void link(View view) {
    }
}
