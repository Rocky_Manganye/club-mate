package com.example.rmanganye.app.views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rmanganye.app.R;
import com.example.rmanganye.app.framework.ClubApplication;
import com.example.rmanganye.app.framework.model.ConfirmCodeRequest;
import com.example.rmanganye.app.framework.model.VerificationResponse;
import com.example.rmanganye.app.framework.services.VerificationService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmNumber extends AppCompatActivity {

    public static final String USER_ID = "USER_ID";
    public static final String CONFIRM_CODE = "CONFIRM_CODE";

    private EditText _etCOdeFive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_number);
        int code = getIntent().getIntExtra(CONFIRM_CODE, 0);
        Toast.makeText(getBaseContext(),String.valueOf(code),Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Button _btnConfirm = (Button) findViewById(R.id.btn_verify);

        _etCOdeFive = (EditText) findViewById(R.id.code_five);
        final EditText _etCOdeFour = (EditText) findViewById(R.id.code_four);
        final EditText _etCOdeThree = (EditText) findViewById(R.id.code_three);
        final EditText _etCOdeTwo = (EditText) findViewById(R.id.code_two);
        final EditText _etCOdeONe = (EditText) findViewById(R.id.code_one);
        final SharedPreferences sharedPreferences = getSharedPreferences("confirmed",0);

        String cell = sharedPreferences.getString("USER_CELL","0");

        TextView view = (TextView) findViewById(R.id.cellNumber);
        view.setText(cell);
        _etCOdeONe.requestFocus();
        _etCOdeONe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (_etCOdeONe.getText().toString().length() == 1)     //size as per your requirement
                    _etCOdeTwo.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        _etCOdeTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (_etCOdeTwo.getText().toString().length() == 1)     //size as per your requirement
                    _etCOdeThree.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        _etCOdeThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (_etCOdeThree.getText().toString().length() == 1)     //size as per your requirement
                    _etCOdeFour.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        _etCOdeFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (_etCOdeFour.getText().toString().length() == 1)     //size as per your requirement
                    _etCOdeFive.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code1 = _etCOdeFive.getText().toString();
                String code2 = _etCOdeFive.getText().toString();
                String code3 = _etCOdeFive.getText().toString();
                String code4 = _etCOdeFive.getText().toString();
                String code5 = _etCOdeFive.getText().toString();


                if (code1.isEmpty() || code2.isEmpty() || code3.isEmpty() || code4.isEmpty() || code5.isEmpty()) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(ConfirmNumber.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                    builder.setTitle("Error");
                    builder.setMessage("Provide all fields");
                    builder.setPositiveButton("OK", null);
                    builder.show();
                } else {
                    //send code to server
                    String finalCOde = _etCOdeONe.getText().toString() + _etCOdeTwo.getText().toString() + _etCOdeThree.getText().toString()
                            + _etCOdeFour.getText().toString() + _etCOdeFive.getText().toString();
                    int userId = getIntent().getIntExtra(USER_ID, 0);
                    int confirmCode = getIntent().getIntExtra(CONFIRM_CODE, 0);

                    ConfirmCodeRequest model = new ConfirmCodeRequest(finalCOde, userId, confirmCode);
                    VerificationService service = ClubApplication.getRetrofit().create(VerificationService.class);
                    Call<VerificationResponse> join = service.confirm(model);
                    join.enqueue(new Callback<VerificationResponse>() {
                        @Override
                        public void onResponse(Call<VerificationResponse> call, Response<VerificationResponse> response) {
                            if (response.isSuccessful()) {
                                if (response.body().isVerified()) {

                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putBoolean("confirmed", true);
                                    editor.apply();
                                    startActivity(new Intent(ConfirmNumber.this, Dashboard.class));
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmNumber.this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
                                    builder.setTitle("Error");
                                    builder.setMessage("Invalid Code");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            _etCOdeFive.setText("");
                                            _etCOdeFour.setText("");
                                            _etCOdeThree.setText("");
                                            _etCOdeTwo.setText("");
                                            _etCOdeONe.setText("");
                                            _etCOdeONe.requestFocus();
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<VerificationResponse> call, Throwable t) {
                            Toast.makeText(getBaseContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

    }
}
