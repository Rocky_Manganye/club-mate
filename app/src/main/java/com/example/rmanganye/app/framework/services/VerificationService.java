package com.example.rmanganye.app.framework.services;

import com.example.rmanganye.app.framework.model.ConfirmCodeRequest;
import com.example.rmanganye.app.framework.model.VerificationResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by rmanganye on 2017/06/04.
 */

public interface VerificationService {
    @POST("verify.php")
    Call<VerificationResponse> confirm (@Body ConfirmCodeRequest request);
}
