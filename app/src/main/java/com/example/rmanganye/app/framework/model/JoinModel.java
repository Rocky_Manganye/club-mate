package com.example.rmanganye.app.framework.model;

/**
 * Created by rmanganye on 2017/06/04.
 */

public class JoinModel {

    private String cellNumber;
    private String username;

    public JoinModel() {
    }

    public JoinModel(String cellNumber, String username) {
        this.cellNumber = cellNumber;
        this.username = username;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
