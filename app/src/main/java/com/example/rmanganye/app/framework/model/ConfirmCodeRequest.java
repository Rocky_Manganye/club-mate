package com.example.rmanganye.app.framework.model;

/**
 * Created by rmanganye on 2017/06/04.
 */

public class ConfirmCodeRequest {
    private String code;
    private int userId;
    private int confirmCode;


    public ConfirmCodeRequest(String code, int userId, int confirmCode) {
        this.code = code;
        this.userId = userId;
        this.confirmCode = confirmCode;
    }

    public ConfirmCodeRequest() {
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getConfirmCode() {
        return confirmCode;
    }

    public void setConfirmCode(int confirmCode) {
        this.confirmCode = confirmCode;
    }
}
