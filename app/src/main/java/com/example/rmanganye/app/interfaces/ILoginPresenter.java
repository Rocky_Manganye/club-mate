package com.example.rmanganye.app.interfaces;

public interface ILoginPresenter {
    void validateCredentials(String username, String password);

    void onDestroy();
}

