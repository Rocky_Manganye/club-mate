package com.example.rmanganye.app.interfaces;

public interface IMainPresenter {
    void onResume();

    void onItemClicked(int position);

    void onDestroy();

    void hideProgress();
}
