package com.example.rmanganye.app.framework.model;

/**
 * Created by rmanganye on 2017/06/04.
 */

public class VerificationResponse {
    public boolean verified;

    public VerificationResponse() {
    }

    public VerificationResponse(boolean verified) {
        this.verified = verified;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
