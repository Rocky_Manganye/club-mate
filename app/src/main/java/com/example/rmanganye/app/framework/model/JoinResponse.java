package com.example.rmanganye.app.framework.model;

/**
 * Created by rmanganye on 2017/06/04.
 */

public class JoinResponse {
    private String cellNumber ;
    private String username ;
    private int id ;
    private int code ;

    public JoinResponse() {
    }


    public JoinResponse(String cellNumber, String username, int id, int code) {
        this.cellNumber = cellNumber;
        this.username = username;
        this.id = id;
        this.code = code;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
