package com.example.rmanganye.app.framework;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rmanganye on 2017/06/04.
 */

public class ClubApplication extends Application {

    private static Retrofit _mRetroFit;

    public static Retrofit getRetrofit() {
        if(_mRetroFit == null)
            _mRetroFit = new Retrofit
                    .Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://patskin.co.za/api/")
                    .build();
        return _mRetroFit;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _mRetroFit = new Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://patskin.co.za/api/")
                .build();
    }
}
