package com.example.rmanganye.app.framework.services;

import com.example.rmanganye.app.framework.model.JoinModel;
import com.example.rmanganye.app.framework.model.JoinResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by rmanganye on 2017/06/04.
 */

public interface JoinService {
    @POST("test.php")
    Call<JoinResponse> join(@Body JoinModel model);
}
